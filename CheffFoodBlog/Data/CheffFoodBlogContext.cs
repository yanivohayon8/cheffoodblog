﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CheffFoodBlog.Models;

namespace CheffFoodBlog.Models
{
    public class CheffFoodBlogContext : DbContext
    {
        public CheffFoodBlogContext (DbContextOptions<CheffFoodBlogContext> options)
            : base(options)
        {
        }

        public DbSet<CheffFoodBlog.Models.Cheff> Cheff { get; set; }

        public DbSet<CheffFoodBlog.Models.Recipe> Recipe { get; set; }

        public DbSet<CheffFoodBlog.Models.Comment> Comment { get; set; }

        public DbSet<CheffFoodBlog.Models.Map> Map { get; set; }
    }
}
