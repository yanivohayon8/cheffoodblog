﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CheffFoodBlog.Models;

namespace CheffFoodBlog.Controllers
{
    public class CheffsController : Controller
    {
        private readonly CheffFoodBlogContext _context;

        public CheffsController(CheffFoodBlogContext context)
        {
            _context = context;
        }

        // GET: Cheffs
        public async Task<IActionResult> Index()
        {
            return View(await _context.Cheff.ToListAsync());
        }

        public IActionResult Filter(string CheffFirstName, string ChefflastName,string placeOfBirth )
        {
            var results = _context.Cheff.AsQueryable();

            // recipe query by cheff last name and first name
            if (!string.IsNullOrEmpty(CheffFirstName) && !string.IsNullOrEmpty(ChefflastName))
            {
                // query join between cheffs and recipes
                results = from cheff in _context.Cheff
                          where cheff.LastName == ChefflastName && cheff.FirstName == CheffFirstName
                          select cheff;//,Name,CheffID,FirstName,LastName;
            }

            // recipe query by either last name or either first name
            if (!string.IsNullOrEmpty(CheffFirstName) || !string.IsNullOrEmpty(ChefflastName))
            {
                // query join between cheffs and recipes
                results = from cheff in _context.Cheff
                          where cheff.LastName == ChefflastName || cheff.FirstName == CheffFirstName
                          select cheff;//,Name,CheffID,FirstName,LastName;
            }

            if(!string.IsNullOrEmpty(placeOfBirth))
            {
                results = results.Where(currCheff => currCheff.PlaceOfBirth == placeOfBirth);
            }

            return View("index", results);
        }


        // GET: Cheffs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cheff = await _context.Cheff
                .FirstOrDefaultAsync(m => m.ID == id);
            if (cheff == null)
            {
                return NotFound();
            }

            return View(cheff);
        }

        // GET: Cheffs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Cheffs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,FirstName,LastName,BirthDay,PlaceOfBirth")] Cheff cheff)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cheff);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cheff);
        }

        // GET: Cheffs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cheff = await _context.Cheff.FindAsync(id);
            if (cheff == null)
            {
                return NotFound();
            }
            return View(cheff);
        }

        // POST: Cheffs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,FirstName,LastName,BirthDay,PlaceOfBirth")] Cheff cheff)
        {
            if (id != cheff.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cheff);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CheffExists(cheff.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cheff);
        }

        // GET: Cheffs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cheff = await _context.Cheff
                .FirstOrDefaultAsync(m => m.ID == id);
            if (cheff == null)
            {
                return NotFound();
            }

            return View(cheff);
        }

        // POST: Cheffs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cheff = await _context.Cheff.FindAsync(id);
            _context.Cheff.Remove(cheff);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }


        private bool CheffExists(int id)
        {
            return _context.Cheff.Any(e => e.ID == id);
        }
    }
}
