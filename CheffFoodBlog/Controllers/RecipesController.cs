﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CheffFoodBlog.Models;

namespace CheffFoodBlog.Controllers
{
    public class RecipesController : Controller
    {
        private readonly CheffFoodBlogContext _context;

        public RecipesController(CheffFoodBlogContext context)
        {
            _context = context;
        }

        // GET: Recipes
        public async Task<IActionResult> Index()
        {
            return View(await _context.Recipe.ToListAsync());
        }

        // GET: Recipes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recipe = await _context.Recipe
                .FirstOrDefaultAsync(m => m.RecipeID == id);
            if (recipe == null)
            {
                return NotFound();
            }

            return View(recipe);
        }

        // GET: Recipes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Recipes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RecipeID,Name,CheffID,DateCreation,CreationProcess")] Recipe recipe)
        {
            if (ModelState.IsValid)
            {
                _context.Add(recipe);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(recipe);
        }

        // GET: Recipes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recipe = await _context.Recipe.FindAsync(id);
            if (recipe == null)
            {
                return NotFound();
            }
            return View(recipe);
        }

        // POST: Recipes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RecipeID,Name,CheffID,DateCreation,CreationProcess")] Recipe recipe)
        {
            if (id != recipe.RecipeID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(recipe);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RecipeExists(recipe.RecipeID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(recipe);
        }

        // GET: Recipes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recipe = await _context.Recipe
                .FirstOrDefaultAsync(m => m.RecipeID == id);
            if (recipe == null)
            {
                return NotFound();
            }

            return View(recipe);
        }

        // POST: Recipes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var recipe = await _context.Recipe.FindAsync(id);
            _context.Recipe.Remove(recipe);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RecipeExists(int id)
        {
            return _context.Recipe.Any(e => e.RecipeID == id);
        }

        //https://localhost:44313/Recipes/Filter?RecipeName=yaniv

        public ActionResult Filter(string RecipeName, string CheffFirstName, string ChefflastName)
        {
            var results = _context.Recipe.AsQueryable();

            //_context.Recipe.AsQueryable();
            //var results;

            // recipe query by cheff last name and first name
            if (!string.IsNullOrEmpty(CheffFirstName) && !string.IsNullOrEmpty(ChefflastName))
            {
                // query join between cheffs and recipes
                results = from recipe in _context.Recipe
                          join cheff in _context.Cheff
                          on recipe.CheffID equals cheff.ID
                          where cheff.LastName == ChefflastName  && cheff.FirstName == CheffFirstName
                          select recipe;//,Name,CheffID,FirstName,LastName;
            }

            // recipe query by either last name or either first name
            if (!string.IsNullOrEmpty(CheffFirstName) || !string.IsNullOrEmpty(ChefflastName))
            {
                // query join between cheffs and recipes
                results = from recipe in _context.Recipe
                          join cheff in _context.Cheff
                          on recipe.CheffID equals cheff.ID
                          where cheff.LastName == ChefflastName || cheff.FirstName == CheffFirstName
                          select recipe;//,Name,CheffID,FirstName,LastName;
            }

            // if we want to filter by recipe Name
            if (!string.IsNullOrEmpty(RecipeName))
            {
                results = results.Where(currRecipe => currRecipe.Name == RecipeName);
            }   


            //return View("Index",results);
            return View("Index", results);
        }

        public ActionResult GroupByCheff()
        {
            var totalRecipes = from recipe in _context.Recipe
                               group recipe by recipe.CheffID into Group
                               join cheff in _context.Cheff on Group.Key equals cheff.ID
                               select new GroupByCheffModel()
                               {
                                   CheffFirstName = cheff.FirstName,
                                   CheffLasttName = cheff.LastName,
                                   CheffID = cheff.ID,
                                   TotalRecipes = Group.Sum(p => 1)
                               };

            return View(totalRecipes.ToList());
        }


    }
}
