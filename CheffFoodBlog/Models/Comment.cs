﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CheffFoodBlog.Models
{
    public class Comment
    {
        [Key]
        public int CommentID { get; set; }
        public int RecipeID { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string AuthorName { get; set; }
        public string Content { get; set; }
        public virtual Recipe Recipe { get; set; }

    }
}
