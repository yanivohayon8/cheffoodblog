﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace CheffFoodBlog.Models
{
    public class Recipe
    {
        [Key]
        public int RecipeID { get; set; }

        public string Name { get; set; }

        public int CheffID { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        public DateTime DateCreation { get; set; }

        public string CreationProcess { get; set; }

        public virtual Cheff Cheff { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }

    public class GroupByCheffModel
    {
        public int CheffID { get; set; }

        public string CheffFirstName { get; set; }

        public string CheffLasttName { get; set; }

        public int TotalRecipes { get; set; }
    }
}
